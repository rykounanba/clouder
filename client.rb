#!/usr/bin/env ruby

class Client
  # client class for running a soundcloud updater
  require 'rubygems'
  require 'socket'
  require 'getoptlong'
  require 'soundcloud'
  require 'yaml'
  require 'json'
  require 'popen4'
  require 'fileutils'
  require 'rykou'
  require_relative 'download.rb'
  require_relative 'tagger.rb'
  require_relative 'converter.rb'

  # TODO: improve the outputs
  # TODO: improve the mailing with playlists

  attr_accessor :verbose
  def initialize(args)
    # initlializing some values and starting the program with a token check
    @logger = Rykou::Logging.new(verbose: args[:verbose])
    @logger.log_path :'/home/rykou/development/clouder/client.log'

    @configpath = File.join(Dir.home, '.config/clouder/config.yaml')
    @config = read_config

    @client_id = '2e675c4b5bab63da046c38e57318c9b0'

    @downloader = Download.new
    @downloader.client_id = @client_id
    @downloader.logger = @logger

    @tagger = Tagger.new

    @converter = Convert.new

    @downloader.tagger = @tagger
    @downloader.converter = @converter

    welcome
    read_token
  end

  def welcome
    @logger.print "Welcome to clouder, clouder is starting up."
    @logger.debug "Verbose mode active"
  end

  def read_token
    # checks to see if our config already has a token stored.
    # False: authorize the program.
    # True: authenticate with the current token.
    begin
      data = Rykou::Yaml.load(@configpath, false)
      @logger.debug data
      if data['account'].nil? then authorize else authenticate(data['account']['access_token']) end
    rescue NoMethodError => e
      @logger.error e
    rescue StandardError => e
      @logger.error e
    end
  end

  def read_config
    # read the config that is stored in our yaml.
    begin
      data = Rykou::Yaml.load(@configpath, false)
      return data['config'] unless data['config'].nil?
    rescue NoMethodError
      @logger.debug "Config file is empty"
    end
  end


  def authorize
    # authorizing the program trough 2 factor authorization
    puts "Authorizing the client, accept the connection with soundcloud and restart the client!"
    client = Soundcloud.new(client_id: @client_id,
                            client_secret: '70d8ca1b947cd503b1642d342a405161',
                            redirect_uri: 'test://test/callback')
    @logger.print client.authorize_url(scope: 'non-expiring')
    status = POpen4::popen4("xdg-open '#{client.authorize_url(scope: 'non-expiring')}'") do |stdout, stdin, stderr, pid |
        p stdout.readline
    end
    @logger.print "Goodbye!"
  end

  def authenticate(token)
    # authenticate the user with soundcloud
    @client = Soundcloud.new(access_token: token)
    current_user = @client.get('/me')
    @logger.print("Currently logged into account: #{current_user['username']}")
    @downloaded_a_track = false
    following = get_followings(token)
    update(following)
    if @downloader.downloaded?
      message = "Hello Rykou,\nI have downloaded some tracks for you:\n"
      @downloader.downloaded_files.each do |filename|
        message += "#{filename}\n"
      end
      message += "\nThat is a total of #{@downloader.downloaded_files.length}.\nHave a nice day<3!"
      @logger.mail(subject: "Downloaded song of the internet for you", message: message, mail: "rykounanba@vivaldi.net")
    end
  end

  def update(following)
    # the loop for the client
    following.each do |accountdata|
      @logger.print "Current artist: #{accountdata.fetch('username')}"
      change_directory(accountdata)
      tracks = list_tracks(accountdata)
      tracks.each do |track|
        if track.fetch('downloadable')
          @downloader.internal track
        else
          @downloader.external track
        end
      end
    end
  end

  def get_followings(token, href=nil, previousdata=[])
     Net::HTTP.start("api.soundcloud.com") do |http|
      if href.nil?
        response = http.get("/me/followings?oauth_token=#{token}&format=json")
        jsondata = JSON.parse(response.body)
        previousdata.concat jsondata["collection"]
      else
        response = http.get("#{href.split(".com")[1]}")
        jsondata = JSON.parse(response.body)
        previousdata.concat jsondata["collection"]
      end
      get_followings(token, jsondata['next_href'], previousdata) unless jsondata['next_href'].nil?
      return previousdata
    end
  end

  def list_tracks(profile)
    tracks = @client.get("/users/#{profile['id']}/tracks")
  end

  def filter_characters(string)
    string.delete("'").delete('?').delete('*').delete('⊕').delete('/')
  end

  def parse_directory(format, profile)
    #parses a directory based upon the format the user wants
    return Dir.getwd if @config['directory'].nil?
    if format.include? '$DIRECTORY'
      format = format.gsub('$DIRECTORY', @config['directory'])
    end
    if format.include? '$ARTIST'
      format = format.gsub('$ARTIST', profile['username'].gsub('/',''))
    end
    format
  end

  def change_directory(profile)
    return if profile.nil?
    begin
      dirmap = read_dirmap
      unless dirmap.include? profile.fetch('id')
        add_to_dirmap profile
        dirmap = read_dirmap
      end
      location = nil
      dirmap.each do |entry|
        location = entry.fetch(:location) if entry.fetch(:id) == profile.fetch('id')
      end
      #parse_directory(@config['format'], profile)
      raise Exception if location.nil?
      Dir.chdir(location)
      @logger.debug "current directory is: #{Dir.getwd}"
    rescue Errno::ENOENT
      create_directory(profile)
      change_directory(profile)
    end
  end

  def create_directory(profile)
    return if profile.nil?
    Dir.mkdir(parse_directory(@config['format'], profile), 0700)
  end

  def read_dirmap
    data = Rykou::Yaml.load(@configpath, false)
    data['dirmap'] = [] if data.fetch('dirmap').nil?
    data['dirmap']
  end

  def add_to_dirmap(profile)
    data = Rykou::Yaml.load(@configpath, false)
    data['dirmap'] = [] if data.fetch('dirmap').nil?
    data['dirmap'].each do |hash|
      return if hash.values.include? profile.fetch('id')
    end
    data['dirmap'].push({id: profile.fetch('id'), location: parse_directory(@config.fetch('format'), profile)})
    Rykou::Yaml.save(@configpath, data, false)
  end
end

if __FILE__ == $PROGRAM_NAME
  #initial option values
  verbose = false

  opts = GetoptLong.new(
    ['--help', '-h', GetoptLong::NO_ARGUMENT],
    ['--send', '-s', GetoptLong::REQUIRED_ARGUMENT],
    ['--verbose', '-v', GetoptLong::NO_ARGUMENT]
  )

  opts.each do |opt, arg|
    case opt
    when '--help'
      puts '--help : print this help menu\n
            --verbose : print all debugging messages'
    when '--verbose'
     verbose = true
    when '--send'
      parsed = arg.split('#access_token=')[1].split('&')[0]
      configpath = File.join(Dir.home, '.config/clouder/config.yaml')
      config_data = Rykou::Yaml.load(configpath, false)
      if config_data['account'].nil?
        config_data['account'] = {}
        config_data['account']['access_token'] = parsed
      end
      Rykou::Yaml.save(configpath, config_data, false)
      exit
    end
  end
  client = Client.new(verbose: verbose)
end

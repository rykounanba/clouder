class Convert

  def initialize()
  end

  attr_accessor :batch, :target, :external
  def convert(args)
    return unless args[:batch]
    return unless args[:target]
    begin
      args[:batch].each do |file|
        return unless File.file? file
        filename = file.reverse.split('.', 2)[1].reverse
        next if File.file? "#{filename}.#{args[:target]}"
        status = POpen4::popen4("ffmpeg -i \"#{file}\" -acodec #{args[:target]} -b:a 48000 -vbr on -compression_level 10 '#{filename.chomp('.mp3')}.#{args[:target]}'") do |stdout, stderr, stdin, pid|
          p stderr.read
        end
        # The exitstatus one signifies an error in the converting process of ffmpeg
        File.delete(file) unless file.include? '.opus' unless status.exitstatus == 1
      end
    rescue NoMethodError => e
      binding.pry
    rescue Exception => e
      binding.pry
    end
  end
end

# a class created for downloading the contents requested by the clouder client
class Download
  require 'net/http'
  require 'pry'
  require 'terminfo'

  attr_writer :client_id
  attr_writer :tagger
  attr_writer :converter
  attr_writer :logger

  #TODO: make downloads go asynchronous

  def initialize
  end

  def downloaded?
    return false if defined?(@downloaded_a_track).nil?
    @downloaded_a_track
  end

  def downloaded_files
    @downloaded_tracks
  end

  # TODO: visually improve this
  def print_progress(trackname, currentsize, fullsize, inline=true)
    percentage = currentsize * 10000 / fullsize / 100
    amount = (percentage * 2 / 10).floor
    innerprogressbar = '                    '
    amount.times do
      innerprogressbar.sub! ' ', '='
    end
    progressbar = "[#{innerprogressbar}]"
    justify_size = `tput cols`.to_i - (progressbar.to_s.length - (trackname.length)) - 20
    if inline
      print "\t#{trackname}:" + "#{progressbar}".rjust(justify_size) + "\r"
    else
      print "\t#{trackname}:"
      print "#{progressbar}".rjust(justify_size) + "\n"
    end
  end

  # Internal downloader for soundcloud songs
  def internal(track)
    return if track.nil?
    return if exists?(track) == true
    # The projected song does not exists
    # Start the downloading procedure
    filename = "#{track['user']['username'].delete('/')} - "\
    "#{filter_characters(track['title'])}.#{track['original_format']}"
    filename = filter_characters(filename)
    file = open(filename, 'wb')
    host, path = split_host(track['download_url'])
    begin
      Net::HTTP.start(host) do |http|
        response = http.get("/#{path}?client_id=#{@client_id}")
        url = response['location']
        raise Rykou::UnauthorizedError if response.is_a? Net::HTTPUnauthorized
        url = response['location'] if response.is_a? Net::HTTPFound
        host, path = split_host(url)
        Net::HTTP.start(host) do |innerhttp|
          innerhttp.request_get("/#{path}") do |resp|
            fullsize = resp['content-length'].to_i
            p fullsize
            currentsize = 0
            resp.read_body do |segment|
              currentsize += segment.bytesize
              print_progress(track['title'], currentsize, fullsize)
              file.write(segment)
            end
          end
        end
      end
    rescue NoMethodError => e
      @logger.error e
    rescue Rykou::UnauthorizedError => e
      @logger.debug("unable to dowload the file, most likely download limit reached!")
      return external(track)
    rescue Net::HTTPBadRequest => e
      @logger.error e
    ensure
      file.close
    end
    print_progress(track['title'], 100, 100, false)
    projected_filename = "#{track['user']['username'].delete('/')} - "\
                         "#{filter_characters(track['title'])}.opus"
    @converter.convert(batch: [filename], target: 'opus')
    @tagger.add_meta_data(track, projected_filename)


    @downloaded_a_track ||= true
    @downloaded_tracks ||= []
    @downloaded_tracks.push projected_filename
  end

  def external(track)
    return if track.nil?
    return if exists?(track) == true
    filename = "#{track['user']['username'].delete('/')} - "\
               "#{filter_characters(track['title'])}.#{track['original_format']}"
    filename = filter_characters(filename)
    #BUG: -add-meta-data seems to fuck with us
    #no chache dir makes sure that songs not get stored in a chache location and not in the music folder
    status = POpen4::popen4("youtube-dl '#{track['permalink_url']}' --restrict-filenames --no-cache-dir -o '#{filename}' -v") do |stdout, stdin, stderr, pid|
       return if stdout.read.strip.include? "has already been downloaded"
    end
    return if status.exitstatus == 1
    @converter.convert(batch: [filename], target: 'opus', external: true)

    @downloaded_a_track ||= true
    @downloaded_tracks ||= []
    @downloaded_tracks.push filename
  end

  private

  def exists?(track)
    # checks if the expected outcome already exists within the filesystem
    projected_filename = "#{track['user']['username'].delete('/')} - "\
                         "#{filter_characters(track['title'])}.opus"
    projected_filename = filter_characters(projected_filename)
    File.file? projected_filename
  end

  def filter_characters(string)
    string.delete("'").delete('?').delete('*').delete('⊕').delete('/').delete('$')
  end

  def split_host(url)
    url.split(%r{/}, 4)[2...4]
  end
end

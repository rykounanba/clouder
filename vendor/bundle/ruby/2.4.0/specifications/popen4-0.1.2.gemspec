# -*- encoding: utf-8 -*-
# stub: popen4 0.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "popen4".freeze
  s.version = "0.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["John-Mason P. Shackelford".freeze]
  s.date = "2009-01-03"
  s.description = "POpen4 provides the Rubyist a single API across platforms for executing a command in a child process with handles on stdout, stderr, stdin streams as well as access to the process ID and exit status.".freeze
  s.email = "john-mason@shackelford.org".freeze
  s.extra_rdoc_files = ["CHANGES".freeze, "LICENSE".freeze, "README.rdoc".freeze]
  s.files = ["CHANGES".freeze, "LICENSE".freeze, "README.rdoc".freeze]
  s.homepage = "http://github.com/shairontoledo/popen4".freeze
  s.rubygems_version = "2.6.11".freeze
  s.summary = "Open4 cross-platform".freeze

  s.installed_by_version = "2.6.11" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<Platform>.freeze, [">= 0.4.0"])
      s.add_runtime_dependency(%q<open4>.freeze, [">= 0.4.0"])
    else
      s.add_dependency(%q<Platform>.freeze, [">= 0.4.0"])
      s.add_dependency(%q<open4>.freeze, [">= 0.4.0"])
    end
  else
    s.add_dependency(%q<Platform>.freeze, [">= 0.4.0"])
    s.add_dependency(%q<open4>.freeze, [">= 0.4.0"])
  end
end

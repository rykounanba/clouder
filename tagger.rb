# A tagging class that tags the information of the songs to the downloaded files
class Tagger
  require 'taglib'
  def initialize
  end

  #TODO: alter the flow to converting before adding the metadata.
  # This is a way to get all files to have the same Tagging containers.
  # then the add meta data method can check wich type the file is
  # and choose the appropiate container and add relevant data.
  # ID3v2 for mp3
  # vorbis containers for OGG, Opus
  # This will be on hold because TagLib does not seem to support Ogg::Opus yet

  def add_meta_data(trackdata, file)
    TagLib::FileRef.open(file) do |fileref|
      unless fileref.null?
        tag = fileref.tag
        tag.title = trackdata['title']
        tag.artist = trackdata['user']['username']
        tag.genre = trackdata['genre']
        tag.album = "#{trackdata['user']['username']} collection"
        tag.comment = trackdata['description']
        tag.year = trackdata['release_year'].to_i
        fileref.save
      end
    end
  end

  def add_album_art
    # TODO: create this function and implement it
  end

  def add_lyrics
    # TODO: create this function and implement it
  end
end
